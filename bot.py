import asyncio
import requests

import discord
from discord.ext import commands


class FriendlyFireBot(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    
    @commands.Cog.listener()
    async def on_ready(self):
        print('=== Bot started ===')
        print('Discord PY Version: {}'.format(discord.__version__))
        print('Bot Name:           {}'.format(self.bot.user.name))
        print('Bot ID:             {}'.format(self.bot.user.id))
    

    @commands.command(aliases=['ps'])
    async def pstats(self, ctx, *players):
        if len(players) == 0:
            players = (ctx.message.author.nick if ctx.message.author.nick else ctx.message.author.name,)

        # Remove all duplicates
        players = tuple(set([p.lower() for p in players]))

        for player in players:
            data = requests.get(f'https://api.wynncraft.com/v2/player/{player}/stats').json()

            if data['code'] != 200:
                embed = discord.Embed(
                    title=f'Error - {data["message"]}',
                    description='Please check the command and try again',
                    colour=discord.Colour.red(),
                )

                await ctx.send(embed=embed)

                return
            
            data = data['data'][0]
            player = data['username']

            forum = requests.get(f'https://api.wynncraft.com/forums/getForumId/{player}').json()

            if 'username' in forum:
                forum = f'https://forums.wynncraft.com/members/{forum["username"]}.{forum["id"]}/'
                forum = f'[<:wynn:572007607817207808>**Wynncraft Forum**]({forum})'
            else:
                forum = '<:wynn:572007607817207808>**Wynncraft Forum**: Not Linked'

            embed = discord.Embed(
                title=f'**{player} - Stats**',
                description=(
                    f'[<:wynn:572007607817207808>**Wynncraft Profile**](https://wynncraft.com/stats/player/{player})\n'
                    f'{forum}'
                ),
                colour=discord.Colour.green(),
            )

            embed.set_thumbnail(url=f'https://visage.surgeplay.com/head/256/{data["uuid"]}')

            if data['rank'] == 'Player' and data['meta']['tag']['value']:
                # 'HERO' gets normalized to 'Hero'
                rank = data['meta']['tag']['value'].capitalize()

            elif data['rank'] != 'Player':
                rank = data['rank']
            
            else:
                rank = data['rank']
            
            # I dont know why its 4.7, i just took that number from the js on wynncraft.com
            playtime = int(data['meta']['playtime'] / 60 * 4.7)

            location = data['meta']['location']['server'] if data['meta']['location']['online'] else 'Offline'

            embed.add_field(
                name='**General**',
                value=(
                    f'**Rank:** {rank}\n'
                    #f'**UUID:** {data["uuid"]}\n'
                    f'**Guild:** {data["guild"]["name"]}\n'
                    f'**Playtime:** {playtime} hours\n'
                    f'**Location:** {location}\n'
                ),
                inline=False,
            )

            # Only show first Character of the Player (usually the main)
            wclass = data['classes'][0]

            # Class Names like 'mage2' get normalized to 'Mage'
            wclass_name = ''.join(i for i in wclass['name'] if not i.isdigit()).capitalize()
            
            playtime = int(wclass['playtime'] / 60 * 4.7)

            embed.add_field(
                name=f'**Main Character:** {wclass_name}',
                value=(
                    '```ml\n' +
                    'Level:        {:11d}\n'.format(wclass['level']) +
                    'Combat Level: {:11d}\n'.format(wclass['professions']['combat']['level']) +
                    'Playtime:     {:9d} h\n'.format(playtime) +
                    'Mobs Killed:  {:11d}\n'.format(wclass['mobsKilled']) +
                    'Chests Found: {:11d}\n'.format(wclass['chestsFound']) +
                    'Blocks Walked:{:11d}\n'.format(wclass['blocksWalked']) +
                    '```'
                ),
                inline=True,
            )
            
            await ctx.send(embed=embed)
    
    
    @commands.command(aliases=['pcs'])
    async def pchars(self, ctx, *players):
        if len(players) == 0:
            players = (ctx.message.author.nick if ctx.message.author.nick else ctx.message.author.name,)

        # Remove all duplicates
        players = tuple(set([p.lower() for p in players]))

        for player in players:
            data = requests.get(f'https://api.wynncraft.com/v2/player/{player}/stats').json()

            if data['code'] != 200:
                embed = discord.Embed(
                    title=f'Error - {data["message"]}',
                    description='Please check the command and try again',
                    colour=discord.Colour.red(),
                )

                await ctx.send(embed=embed)

                return
            
            data = data['data'][0]
            player = data['username']

            embed = discord.Embed(
                title=f'**{player} - All Characters**',
                colour=discord.Colour.green(),
            )

            embed.set_thumbnail(url=f'https://visage.surgeplay.com/head/256/{data["uuid"]}')

            for index in range(len(data['classes'])):
                wclass = data['classes'][index]
                wclass_name = ''.join(i for i in wclass['name'] if not i.isdigit()).capitalize()
                
                playtime = int(wclass['playtime'] / 60 * 4.7)

                embed.add_field(
                    name=f'**[{index}] {wclass_name}**',
                    value=(
                        '```ml\n' +
                        'Level:        {:9d}\n'.format(wclass['level']) +
                        'Combat Level: {:9d}\n'.format(wclass['professions']['combat']['level']) +
                        'Playtime:     {:7d} h\n'.format(playtime) +
                        'Mobs Killed:  {:9d}\n'.format(wclass['mobsKilled']) +
                        'Chests Found: {:9d}\n'.format(wclass['chestsFound']) +
                        'Blocks Walked:{:9d}\n'.format(wclass['blocksWalked']) +
                        '```'
                    ),
                    inline=(not index == 0),
                )
            
            await ctx.send(embed=embed)
    

    @commands.command(aliases=['pc'])
    async def pchar(self, ctx, player=None, index: int=0):
        if player == None:
            player = ctx.message.author.nick if ctx.message.author.nick else ctx.message.author.name

        data = requests.get(f'https://api.wynncraft.com/v2/player/{player}/stats').json()

        if data['code'] != 200:
            embed = discord.Embed(
                title=f'Error - {data["message"]}',
                description='Please check the command and try again',
                colour=discord.Colour.red(),
            )

            await ctx.send(embed=embed)

            return
        
        data = data['data'][0]
        player = data['username']

        wclass = data['classes'][index]
        wclass_name = ''.join(i for i in wclass['name'] if not i.isdigit()).capitalize()
        
        playtime = int(wclass['playtime'] / 60 * 4.7)

        embed = discord.Embed(
            title=f'**{player} - {wclass_name}** [{index}]',
            colour=discord.Colour.green(),
        )

        embed.set_thumbnail(url=f'https://visage.surgeplay.com/head/256/{data["uuid"]}')

        embed.add_field(
            name=f'**General Info**',
            value=(
                '```ml\n' +
                'Level:        {:9d}\n'.format(wclass['level']) +
                'Combat Level: {:9d}\n'.format(wclass['professions']['combat']['level']) +
                'Playtime:     {:7d} h\n'.format(playtime) +
                'Mobs Killed:  {:9d}\n'.format(wclass['mobsKilled']) +
                'Chests Found: {:9d}\n'.format(wclass['chestsFound']) +
                'Blocks Walked:{:9d}\n'.format(wclass['blocksWalked']) +
                'Player Kills: {:9d}\n'.format(wclass['pvp']['kills']) +
                'Player Deaths:{:9d}\n'.format(wclass['pvp']['deaths']) +
                '```'
            ),
            inline=False,
        )

        embed.add_field(
            name='**Skills**',
            value=(
                '```ml\n' +
                'Strength:    {:10d}\n'.format(wclass['skills']['strength']) +
                'Dexterity:   {:10d}\n'.format(wclass['skills']['dexterity']) +
                'Intelligence:{:10d}\n'.format(wclass['skills']['intelligence']) +
                'Defense:     {:10d}\n'.format(wclass['skills']['defense']) +
                'Agility:     {:10d}\n'.format(wclass['skills']['agility']) +
                '```'
            ),
            inline=False,
        )

        embed.add_field(
            name='**Professions**',
            value=(
                '```ml\n' +
                'Farming:       {:8d}\n'.format(wclass['professions']['farming']['level']) +
                'Fishing:       {:8d}\n'.format(wclass['professions']['fishing']['level']) +
                'Mining:        {:8d}\n'.format(wclass['professions']['mining']['level']) +
                'Woodcutting:   {:8d}\n'.format(wclass['professions']['woodcutting']['level']) +
                '\n'
                'Alchemism:     {:8d}\n'.format(wclass['professions']['alchemism']['level']) +
                'Armouring:     {:8d}\n'.format(wclass['professions']['armouring']['level']) +
                'Cooking:       {:8d}\n'.format(wclass['professions']['cooking']['level']) +
                'Jeweling:      {:8d}\n'.format(wclass['professions']['jeweling']['level']) +
                'Scribing:      {:8d}\n'.format(wclass['professions']['scribing']['level']) +
                'Tailoring:     {:8d}\n'.format(wclass['professions']['tailoring']['level']) +
                'Weaponsmithing:{:8d}\n'.format(wclass['professions']['weaponsmithing']['level']) +
                'Woodworking:   {:8d}\n'.format(wclass['professions']['woodworking']['level']) +
                '```'
            ),
            inline=False,
        )
        
        await ctx.send(embed=embed)
