# Friendly Fire Bot
A Discord Bot to check Wynncraft Stats

This Bot is made by the Friendly Fire Guild

## Setup
1. Install the requirements with
`pip install -r requirements.txt`
2. Create a discord App on the [Discord Developer Portal](https://discordapp.com/developers/applications/)
3. Copy the token of your discord bot into `main.py`
4. Invite the bot to your Discord Server: [Link](https://discordapp.com/oauth2/authorize?client_id=<your_bot_id>&scope=bot) (make sure to insert your bot ID)

## Commands
### !pstats
`!pstats [<player>...]` shows general information about the player and the stats of their highest level character.
* `player` defaults to the nickname / username of the discord user

This command can also get executed with `!ps`

<img src="/images/pstats.png"  width="500">

### !pchars
`!pchars [<player>...]` lists all the characters of the player with an index thats used to get more details. (see `!pchar`)
* `player` defaults to the nickname / username of the discord user

This command can also get executed with `!pcs`

<img src="/images/pchars.png"  width="500">

### !pchar
`!pchar [<player>] [<index>]` outputs more details about the given character. Use `!pchars` to get the index of the character you want to view
* `player` defaults to the nickname / username of the discord user
* `index` defaults to 0 as it is most likely the main character (since it has the highest overall level)

This command can also get executed with `!pc`

<img src="/images/pchar.png"  width="500">
